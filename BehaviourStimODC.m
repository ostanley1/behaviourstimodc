% Clear the workspace
close all;
clearvars;
sca;

Screen('Preference', 'SkipSyncTests', 1); 

% Here we call some default settings for setting up Psychtoolbox
PsychDefaultSetup(2);

% Get the screen numbers
screens = Screen('Screens');

% Draw to the external screen if avaliable
screenNumber = max(screens);

% Define black and white
white = WhiteIndex(screenNumber);
black = BlackIndex(screenNumber);
grey = white / 2;
red = [1 0 0 1];

% Open an on screen window
[window, windowRect] = PsychImaging('OpenWindow', screenNumber,...
    grey, [], 32, 2, [], [], kPsychNeed32BPCFloat);

% Query the frame duration
ifi = Screen('GetFlipInterval', window) 

% Screen resolution in Y
screenYpix = windowRect(4);

% Number of white/black circle pairs
rcycles = 8;

% Number of white/black angular segment pairs (integer)
tcycles = 24;

% Now we make our checkerboard pattern
xylim = 2 * pi * rcycles;
[x, y] = meshgrid(-xylim: 2 * xylim / (screenYpix - 1): xylim,...
    -xylim: 2 * xylim / (screenYpix - 1): xylim);
at = atan2(y, x);
checks = ((1 + sign(sin(at * tcycles) + eps)...
    .* sign(sin(sqrt(x.^2 + y.^2)))) / 2) * (white - black) + black;
circle = x.^2 + y.^2 <= xylim^2;
checks = circle .* checks + grey * ~circle;

% Now we make this into a PTB texture
radialCheckerboardTexture(1)  = Screen('MakeTexture', window, checks);
radialCheckerboardTexture(2)  = Screen('MakeTexture', window, 1 - checks);

% Here we set the size of the arms of our fixation cross
fixCrossDimPix = 40;
lineWidthPix = 4;

% Now we set the coordinates (these are all relative to zero we will let
% the drawing routine center the cross in the center of our monitor for us)
[xCenter, yCenter] = RectCenter(windowRect);
xCoords = [-fixCrossDimPix fixCrossDimPix 0 0];
yCoords = [0 0 -fixCrossDimPix fixCrossDimPix];
allCoords = [xCoords; yCoords];

% Time we want to wait before reversing the contrast of the checkerboard
checkFlipTimeSecs = 1/8;
checkFlipTimeFrames = round(checkFlipTimeSecs / ifi);
frameCounter = 0; 

% Time we want to wait before reversing the contrast of the checkerboard
checkBlockTimeSecs = 10;
checkBlockTimeFrames = round(checkBlockTimeSecs / ifi);
blockCounter = 0; 

% Time we want to wait before reversing the contrast of the checkerboard
checkGoggleTimeSecs = 20;
checkGoggleTimeFrames = round(checkGoggleTimeSecs / ifi);
goggleOffsetSecs = 5;
goggleCounter = -round(goggleOffsetSecs / ifi)

% Texture cue that determines which texture we will show
textureCue = [1 2];
blockState = [0 1];
goggleState = [1 0];

% Sync us to the vertical retrace
vbl = Screen('Flip', window);

dio = digitalio('mcc',0); % mcc is the measurement computing drivers, 0 is the board number set in InstaCal
addline(dio,1:27,'out'); % add all the output lines
putvalue(dio.line(5), goggleState(1));
putvalue(dio.line(4), goggleState(2));

while ~KbCheck
    % Increment the counter
    frameCounter = frameCounter + 1;
    blockCounter = blockCounter + 1;
    goggleCounter = goggleCounter + 1;

    if blockState(1) ~= 0,
        % Draw our texture to the screen
        Screen('DrawTexture', window, radialCheckerboardTexture(textureCue(1)));
    end
    
    % Flip to the screen
    Screen('DrawLines', window, allCoords, lineWidthPix, red, [xCenter yCenter]  );
    vbl = Screen('Flip', window);

    % Reverse the texture cue to show the other polarity if the time is up
    if frameCounter == checkFlipTimeFrames ,
        textureCue = fliplr(textureCue);
        frameCounter = 0;
    end
    
    if blockCounter == checkBlockTimeFrames ,
        blockState = fliplr(blockState);
        blockCounter = 0;
    end
    
    if goggleCounter == checkGoggleTimeFrames,
        goggleState = fliplr(goggleState);
        putvalue(dio.line(5), goggleState(1));
        putvalue(dio.line(4), goggleState(2)); 
        goggleCounter = 0;
    end
end

% Clear up and leave the building
sca;
delete(dio);
clear('dio');
close all;
clear all; 
